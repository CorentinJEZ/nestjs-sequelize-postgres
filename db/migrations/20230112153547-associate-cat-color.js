'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('CatColors', {
      catId: Sequelize.INTEGER,
      colorId: Sequelize.INTEGER,
      stars: Sequelize.INTEGER,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('CatColors');
  },
};
