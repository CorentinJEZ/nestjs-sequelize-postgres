import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private readonly logger: Logger = new Logger(LoggerMiddleware.name);

  use(req: Request, res: Response, next: NextFunction) {
    const logReqRes = (req, res, next) => {
      const startHrTime = process.hrtime();
      const defaultWrite = res.write;
      const defaultEnd = res.end;
      const chunks = [];

      res.write = (...restArgs) => {
        chunks.push(Buffer.from(restArgs[0]));
        defaultWrite.apply(res, restArgs);
      };

      res.end = (...restArgs) => {
        if (restArgs[0]) chunks.push(Buffer.from(restArgs[0]));
        const body = Buffer.concat(chunks).toString('utf8');

        const elapsedHrTime = process.hrtime(startHrTime);
        const elapsedTimeInMs =
          elapsedHrTime[0] * 1000 + elapsedHrTime[1] / 1e6;
        const reqResInfo = {
          url: req.path,
          path: req.path,
          queryParams: res.query,
          method: req.method,
          requestBody: req.body,
          responseStatus: res.statusCode,
          responseBody: body ? JSON.parse(body) : undefined,
          timestamp: new Date(),
          timeTaken: elapsedTimeInMs,
        };
        if (res.statusCode >= 200 && res.statusCode < 400) {
          this.logger.log(JSON.stringify(reqResInfo));
        } else if (res.statusCode >= 400 && res.statusCode < 500) {
          this.logger.warn(JSON.stringify(reqResInfo));
        } else {
          this.logger.error(JSON.stringify(reqResInfo));
        }

        defaultEnd.apply(res, restArgs);
      };
      next();
    };
    logReqRes(req, res, next);
  }
}
