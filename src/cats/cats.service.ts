import { Injectable, Inject } from '@nestjs/common';
import { Color } from '../colors/color.entity';
import { CATS_REPOSITORY } from '../@core/constants';
import { Cat } from './cat.entity';
import { CreateCatDto } from './createCat.dto';

@Injectable()
export class CatsService {
  constructor(
    @Inject(CATS_REPOSITORY)
    private catsRepository: typeof Cat,
  ) {}

  async findAll(): Promise<Cat[]> {
    return this.catsRepository.findAll<Cat>({ include: Color });
  }

  async find(id: number): Promise<Cat> {
    return this.catsRepository.findByPk<Cat>(id);
  }

  async create(cat: CreateCatDto): Promise<Cat> {
    return new Cat({ name: cat.name, age: cat.age }).save();
  }
}
