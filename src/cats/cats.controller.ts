import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Cat } from './cat.entity';
import { CatsService } from './cats.service';
import { CreateCatDto } from './createCat.dto';

@Controller('cats')
export class CatsController {
  constructor(private readonly catService: CatsService) {}

  @Get()
  findAll(): Promise<Cat[]> {
    return this.catService.findAll();
  }

  @Get(':id')
  find(@Param() params): Promise<Cat> {
    return this.catService.find(params.id);
  }

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  create(@Body() cat: CreateCatDto): Promise<Cat> {
    return this.catService.create(cat);
  }
}
