import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateCatDto {
  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  readonly age: number;

  @IsOptional()
  readonly colorId;
}
