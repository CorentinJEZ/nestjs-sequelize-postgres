import { Table, Column, Model, BelongsToMany } from 'sequelize-typescript';
import { CatColor } from '../catsColors/catColor.entity';
import { Color } from '../colors/color.entity';

@Table({
  timestamps: false,
})
export class Cat extends Model {
  @Column
  declare name: string;

  @Column
  declare age: number;

  @BelongsToMany(() => Color, () => CatColor)
  declare colors: Color[];
}
