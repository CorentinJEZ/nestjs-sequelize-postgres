import { CATS_REPOSITORY } from '../@core/constants';
import { Cat } from './cat.entity';

export const catsProviders = [
  {
    provide: CATS_REPOSITORY,
    useValue: Cat,
  },
];
