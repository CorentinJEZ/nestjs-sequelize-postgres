import { Global, Module, Provider } from '@nestjs/common';

const SERVICE_PROVIDERS: Provider[] = [];
const REPOSITORY_PROVIDERS: Provider[] = [];

@Module({
  imports: [],
  providers: [...SERVICE_PROVIDERS, ...REPOSITORY_PROVIDERS],
  exports: [...SERVICE_PROVIDERS],
})
@Global()
export class CoreModule {}
