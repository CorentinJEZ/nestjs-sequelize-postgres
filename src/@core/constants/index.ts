export const SEQUELIZE = 'SEQUELIZE';
export const DEVELOPMENT = 'development';
export const TEST = 'test';
export const PRODUCTION = 'production';
export const CATS_REPOSITORY = 'CATS_REPOSITORY';
export const COLORS_REPOSITORY = 'COLORS_REPOSITORY';
export const CATCOLOR_REPOSITORY = 'CATCOLOR_REPOSITORY';
