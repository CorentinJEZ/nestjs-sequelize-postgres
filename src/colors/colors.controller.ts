import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Color } from './color.entity';
import { ColorsService } from './colors.service';
import { CreateColorDto } from './createColor.dto';

@Controller('colors')
export class ColorsController {
  constructor(private readonly colorService: ColorsService) {}

  @Get()
  findAll(): Promise<Color[]> {
    return this.colorService.findAll();
  }

  @Get(':id')
  find(@Param() params): Promise<Color> {
    return this.colorService.find(params.id);
  }

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  create(@Body() color: CreateColorDto): Promise<Color> {
    return this.colorService.create(color);
  }
}
