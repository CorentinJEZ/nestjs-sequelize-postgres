import { Injectable, Inject } from '@nestjs/common';
import { Cat } from '../cats/cat.entity';
import { COLORS_REPOSITORY } from '../@core/constants';

import { Color } from './color.entity';
import { CreateColorDto } from './createColor.dto';

@Injectable()
export class ColorsService {
  constructor(
    @Inject(COLORS_REPOSITORY)
    private colorRepository: typeof Color,
  ) {}

  async findAll(): Promise<Color[]> {
    return this.colorRepository.findAll<Color>({ include: Cat });
  }

  async find(id: number): Promise<Color> {
    return this.colorRepository.findByPk<Color>(id);
  }

  async create(color: CreateColorDto): Promise<Color> {
    return new Color({ name: color.name }).save();
  }
}
