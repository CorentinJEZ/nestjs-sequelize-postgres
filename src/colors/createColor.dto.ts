import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateColorDto {
  @IsNotEmpty()
  readonly name: string;

  @IsOptional()
  readonly catId;
}
