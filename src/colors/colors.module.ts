import { Module } from '@nestjs/common';
import { ColorsController } from './colors.controller';
import { colorsProviders } from './colors.providers';
import { ColorsService } from './colors.service';

@Module({
  imports: [],
  controllers: [ColorsController],
  providers: [ColorsService, ...colorsProviders],
})
export class ColorsModule {}
