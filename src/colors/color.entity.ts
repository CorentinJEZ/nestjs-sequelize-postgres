import { Table, Column, Model, BelongsToMany } from 'sequelize-typescript';
import { Cat } from '../cats/cat.entity';
import { CatColor } from '../catsColors/catColor.entity';

@Table({
  timestamps: false,
})
export class Color extends Model {
  @Column
  declare name: string;

  @BelongsToMany(() => Cat, () => CatColor)
  declare cats: Cat[];
}
