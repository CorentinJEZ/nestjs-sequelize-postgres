import { COLORS_REPOSITORY } from '../@core/constants';
import { Color } from './color.entity';

export const colorsProviders = [
  {
    provide: COLORS_REPOSITORY,
    useValue: Color,
  },
];
