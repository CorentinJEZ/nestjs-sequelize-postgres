import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { CatsModule } from './cats/cats.module';
import { ColorsModule } from './colors/colors.module';
import { CatColorsModule } from './catsColors/catColors.module';
import { CoreModule } from './@core/core.module';
import { DatabaseModule } from './@database/database.module';
import { LoggerMiddleware } from './#middleware/logger.middleware';

@Module({
  imports: [
    CoreModule,
    DatabaseModule,
    CatsModule,
    ColorsModule,
    CatColorsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(LoggerMiddleware)

      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
