import { Module } from '@nestjs/common';
import { catsProviders } from '../cats/cats.providers';
import { colorsProviders } from '../colors/colors.providers';
import { CatColorsController } from './catColors.controller';
import { catColorsProviders } from './catColors.providers';
import { CatColorsService } from './catColors.service';

@Module({
  imports: [],
  controllers: [CatColorsController],
  providers: [
    CatColorsService,
    ...catColorsProviders,
    ...colorsProviders,
    ...catsProviders,
  ],
})
export class CatColorsModule {}
