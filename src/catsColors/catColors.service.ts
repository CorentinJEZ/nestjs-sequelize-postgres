import { Injectable, Inject } from '@nestjs/common';
import { Cat } from '../cats/cat.entity';
import { Color } from '../colors/color.entity';
import {
  CATCOLOR_REPOSITORY,
  CATS_REPOSITORY,
  COLORS_REPOSITORY,
} from '../@core/constants';
import { CatColor } from './catColor.entity';
import { CreateCatColorDto } from './createCatColor.dto';

@Injectable()
export class CatColorsService {
  constructor(
    @Inject(CATCOLOR_REPOSITORY)
    private catColorRepository: typeof CatColor,
    @Inject(CATS_REPOSITORY)
    private catRepository: typeof Cat,
    @Inject(COLORS_REPOSITORY)
    private colorRepository: typeof Color,
  ) {}

  async findAll(): Promise<CatColor[]> {
    return this.catColorRepository.findAll<CatColor>();
  }

  async find(id: number): Promise<CatColor> {
    return this.catColorRepository.findByPk<CatColor>(id);
  }

  async create(catColor: CreateCatColorDto): Promise<CatColor> {
    const cat: Cat = await this.catRepository.findByPk(catColor.catId);
    const color: Color = await this.colorRepository.findByPk(catColor.colorId);
    if (!cat && !color) return null;

    const catColorModel = new CatColor({
      stars: catColor.stars,
      catId: cat.id,
      colorId: color.id,
    }).save();
    return catColorModel;
  }
}
