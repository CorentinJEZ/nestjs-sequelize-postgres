import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { Cat } from '../cats/cat.entity';
import { Color } from '../colors/color.entity';

@Table({
  timestamps: false,
})
export class CatColor extends Model {
  @ForeignKey(() => Cat)
  @Column
  declare catId: number;

  @ForeignKey(() => Color)
  @Column
  declare colorId: number;

  @Column
  declare stars: number;
}
