import { CATCOLOR_REPOSITORY } from '../@core/constants';
import { CatColor } from './catColor.entity';

export const catColorsProviders = [
  {
    provide: CATCOLOR_REPOSITORY,
    useValue: CatColor,
  },
];
