import { IsNotEmpty } from 'class-validator';

export class CreateCatColorDto {
  @IsNotEmpty()
  readonly stars: number;

  @IsNotEmpty()
  readonly colorId: number;

  @IsNotEmpty()
  readonly catId: number;
}
