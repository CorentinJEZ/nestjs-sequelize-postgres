import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CatColor } from './catColor.entity';
import { CatColorsService } from './catColors.service';
import { CreateCatColorDto } from './createCatColor.dto';

@Controller('catColors')
export class CatColorsController {
  constructor(private readonly catColorService: CatColorsService) {}

  @Get()
  findAll(): Promise<CatColor[]> {
    return this.catColorService.findAll();
  }

  @Get(':id')
  find(@Param() params): Promise<CatColor> {
    return this.catColorService.find(params.id);
  }

  @Post()
  @UsePipes(new ValidationPipe({ transform: true }))
  create(@Body() catColor: CreateCatColorDto): Promise<CatColor> {
    return this.catColorService.create(catColor);
  }
}
